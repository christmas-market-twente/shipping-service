#!/bin/sh
#echo "$ACTIVEMQ_WEB_USERNAME: $ACTIVEMQ_WEB_PASSWORD, admin" > /apache-activemq-5.15.8/conf/jetty-realm.properties
:> /apache-activemq-5.15.8/conf/jetty-realm.properties
echo "$ACTIVEMQ_BROKER_USERNAME=password" > /apache-activemq-5.15.8/conf/users.properties
sed -i "/<\\/broker>/i <plugins><simpleAuthenticationPlugin><users><authenticationUser username=\"$ACTIVEMQ_BROKER_USERNAME\" password=\"$ACTIVEMQ_BROKER_PASSWORD\" groups=\"users\"/></users></simpleAuthenticationPlugin></plugins>" /apache-activemq-5.15.8/conf/activemq.xml
/apache-activemq-5.15.8/bin/activemq console