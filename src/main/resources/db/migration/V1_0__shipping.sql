CREATE TABLE `shippings` (
    `id` BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    `requested_at` BIGINT UNSIGNED NOT NULL,
    `accepted_at` BIGINT UNSIGNED,
    `shipped_at` BIGINT UNSIGNED,
    `order_id` BIGINT UNSIGNED NOT NULL
);