package nl.utwente.soa.cmt.shippingservice.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@Entity
@Table(name = "shippings")
public class Shipping {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private Long requestedAt;

    private Long acceptedAt;

    private Long shippedAt;

    @NotNull
    private Long orderId;
}
