package nl.utwente.soa.cmt.shippingservice.receiver;

import nl.utwente.soa.cmt.shippingservice.dto.request.ShippingRequestDto;
import nl.utwente.soa.cmt.shippingservice.dto.response.ShipStatusResponseDto;
import nl.utwente.soa.cmt.shippingservice.model.Shipping;
import nl.utwente.soa.cmt.shippingservice.repository.ShippingRepository;
import nl.utwente.soa.cmt.shippingservice.service.ShippingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import java.util.Timer;
import java.util.TimerTask;

@Component
public class OrderReceiver {

    @Autowired
    private ShippingRepository shippingRepository;

    @Autowired
    private ShippingService shippingService;

    @JmsListener(destination = "${queue.shipping}") //containerFactory = "myFactory"
    public void receiveMessage(ShippingRequestDto shippingRequestDto) {
        Shipping shipping = shippingService.createShipping(shippingRequestDto);

        ShipStatusResponseDto acceptResponse = new ShipStatusResponseDto();
        acceptResponse.setOrderId(shipping.getOrderId());
        acceptResponse.setTimestamp(shipping.getAcceptedAt());
        acceptResponse.setStatus("Accepted");
        shippingService.notifyStatus(acceptResponse);

        /* Usually this done after a REST call from the shipping controller,
         * but since implementing all that is out of scope for now we
         * simply set a time-out to test the response and queue. */
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                shippingService.setShipTime(shipping);

                ShipStatusResponseDto shipResponse = new ShipStatusResponseDto();
                shipResponse.setOrderId(shipping.getOrderId());
                shipResponse.setTimestamp(shipping.getShippedAt());
                shipResponse.setStatus("Shipped");
                shippingService.notifyStatus(shipResponse);
            }
        }, 20000);
    }
}
