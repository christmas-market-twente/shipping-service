package nl.utwente.soa.cmt.shippingservice.service;

import nl.utwente.soa.cmt.shippingservice.dto.request.ShippingRequestDto;
import nl.utwente.soa.cmt.shippingservice.dto.response.ShipStatusResponseDto;
import nl.utwente.soa.cmt.shippingservice.model.Shipping;
import nl.utwente.soa.cmt.shippingservice.repository.ShippingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class ShippingService {

    @Autowired
    private JmsTemplate jmsTemplate;

    @Autowired
    private ShippingRepository shippingRepository;

    @Value("${queue.order}")
    private String orderQueue;

    public Shipping createShipping(ShippingRequestDto shippingRequestDto) {
        Shipping shipping = new Shipping();
        shipping.setOrderId(shippingRequestDto.getOrderId());
        shipping.setRequestedAt(shippingRequestDto.getRequestedAt());
        shipping.setAcceptedAt(new Date().getTime() / 1000);
        return shippingRepository.save(shipping);
    }

    public void setShipTime(Shipping shipping) {
        shipping.setShippedAt(new Date().getTime() / 1000);
        shippingRepository.save(shipping);
    }

    public void notifyStatus(ShipStatusResponseDto shipResponse) {
        jmsTemplate.convertAndSend(orderQueue, shipResponse);
    }

}
