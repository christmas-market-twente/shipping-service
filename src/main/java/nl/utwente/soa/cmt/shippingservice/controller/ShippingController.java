package nl.utwente.soa.cmt.shippingservice.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import nl.utwente.soa.cmt.shippingservice.dto.response.ShipStatusResponseDto;
import nl.utwente.soa.cmt.shippingservice.response.ErrorResponse;
import nl.utwente.soa.cmt.shippingservice.service.ShippingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/shippings")
@Tag(name = "Shipping", description = "The Shipping API")
public class ShippingController {

    @Autowired
    private ShippingService shippingService;

    @Operation(summary = "Update the status of a shipping order", description = "Adds the changed status to the Order Queue", tags = {"Shipping"})
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Successful operation"),
        @ApiResponse(responseCode = "400", description = "Invalid Request Body",
            content = @Content(schema = @Schema(implementation = ErrorResponse.class)))})
    @PostMapping("/{id}/ship")
    public void notifyShip(@Parameter(description = "Information on the new status of a shipping order.")
                              @RequestBody ShipStatusResponseDto shipStatusResponseDto) {
        shippingService.notifyStatus(shipStatusResponseDto);
    }
}
