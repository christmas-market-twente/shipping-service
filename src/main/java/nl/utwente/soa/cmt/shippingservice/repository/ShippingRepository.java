package nl.utwente.soa.cmt.shippingservice.repository;

import nl.utwente.soa.cmt.shippingservice.model.Shipping;
import org.springframework.data.repository.CrudRepository;

public interface ShippingRepository extends CrudRepository<Shipping, Long> {

}
