package nl.utwente.soa.cmt.shippingservice.dto.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class ShipStatusResponseDto implements Serializable {

    private Long orderId;

    private Long timestamp;

    private String status;

    public ShipStatusResponseDto(Long orderId, Long timestamp, String status) {
        this.orderId = orderId;
        this.timestamp = timestamp;
        this.status = status;
    }

    public ShipStatusResponseDto() {}
}
